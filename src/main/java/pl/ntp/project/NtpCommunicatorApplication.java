package pl.ntp.project;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import pl.ntp.project.security.JwtFilter;

@SpringBootApplication
public class NtpCommunicatorApplication {

	@Bean
	public FilterRegistrationBean jwtFilter() {
		final FilterRegistrationBean registrationBean = new FilterRegistrationBean();
		registrationBean.setFilter(new JwtFilter());
		registrationBean.addUrlPatterns("/tymczasowo_zablokowane/*");

		return registrationBean;
	}

	public static void main(String[] args) {
		SpringApplication.run(NtpCommunicatorApplication.class, args);
	}

}

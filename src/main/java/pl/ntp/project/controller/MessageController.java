package pl.ntp.project.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.ntp.project.exception.MessageNotFoundException;
import pl.ntp.project.exception.MessageWrongFormatException;
import pl.ntp.project.exception.UserNotFoundException;
import pl.ntp.project.model.Converters;
import pl.ntp.project.model.Message;
import pl.ntp.project.model.User;
import pl.ntp.project.model.dto.MessageDTO;
import pl.ntp.project.model.service.MessageService;
import pl.ntp.project.model.service.UserService;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Łukasz Patro
 * on 18.06.17.
 */
@RestController
@CrossOrigin
@RequestMapping("/users/{userId}/messages")
public class MessageController {

    @Autowired
    MessageService messageService;

    @Autowired
    UserService userService;

    @PostMapping
    public ResponseEntity<MessageDTO> sendMessage(@RequestBody MessageDTO messageDTO, @PathVariable Long userId) {

        User userSentBy;
        User userSentTo;

        try {
            userSentBy = userService.getUserById(userId);
            userSentTo = userService.getUserById(messageDTO.getSentTo());
        } catch (UserNotFoundException e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        Message message = Converters.toMessage(messageDTO);
        message.setSentBy(userSentBy);
        message.setSentTo(userSentTo);
        message.setTimestamp(new Date());
        message.setIsReceived(Boolean.FALSE);

        try {
            messageService.sendMessage(message);
        } catch (MessageWrongFormatException | UserNotFoundException e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity<>(Converters.toMessageDTO(message), HttpStatus.OK);

    }

    @GetMapping("/{messageId}")
    public ResponseEntity<MessageDTO> getMessage(@PathVariable Long userId, @PathVariable Long messageId) {

        Message msg;

        try {
            msg = messageService.getMessage(messageId, userId);
        } catch (UserNotFoundException e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        } catch (MessageNotFoundException e) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }

        return new ResponseEntity<>(Converters.toMessageDTO(msg), HttpStatus.OK);

    }

    @GetMapping("/{userId2}/{page}/{quantity}")
    public ResponseEntity<List<MessageDTO>> getConversation(@PathVariable Long userId, @PathVariable Long userId2, @PathVariable int page, @PathVariable int quantity) {

        User u1, u2;
        List<Message> messages;

        try {
            u1 = userService.getUserById(userId);
            u2 = userService.getUserById(userId2);
            messages = messageService.getConversation(u1.getId(), u2.getId(), page, quantity);
        } catch (UserNotFoundException e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        } catch (MessageNotFoundException e) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }

        List<MessageDTO> messagesDTO = messages.stream().map(Converters::toMessageDTO).collect(Collectors.toList());
        return new ResponseEntity<>(messagesDTO, HttpStatus.OK);

    }

    @GetMapping("/unread/{userId2}/{page}/{quantity}")
    public ResponseEntity<List<MessageDTO>> getConversationUnread(@PathVariable Long userId, @PathVariable Long userId2, @PathVariable int page, @PathVariable int quantity) {

        User u1, u2;
        List<Message> messages;

        try {
            u1 = userService.getUserById(userId);
            u2 = userService.getUserById(userId2);
            messages = messageService.getConversationUnread(u1.getId(), u2.getId(), page, quantity);
        } catch (UserNotFoundException e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        } catch (MessageNotFoundException e) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }

        List<MessageDTO> messagesDTO = messages.stream().map(Converters::toMessageDTO).collect(Collectors.toList());
        return new ResponseEntity<>(messagesDTO, HttpStatus.OK);

    }

    @GetMapping("/unread/{page}/{quantity}")
    public ResponseEntity<List<MessageDTO>> getAllUsersUnreadMessages(@PathVariable Long userId, @PathVariable int page, @PathVariable int quantity) {

        List<Message> messages;
        try {
            messages = messageService.getUsersUnreadMessages(userId, page, quantity);
        } catch (UserNotFoundException e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        } catch (MessageNotFoundException e) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }

        List<MessageDTO> messagesDTO = messages.stream().map(Converters::toMessageDTO).collect(Collectors.toList());

        return new ResponseEntity<>(messagesDTO, HttpStatus.OK);

    }

    @PutMapping("/{messageId}")
    public ResponseEntity<MessageDTO> markAsRead(@PathVariable Long userId, @PathVariable Long messageId) {

        User userSentTo;

        try {
            userSentTo = userService.getUserById(userId);
        } catch (UserNotFoundException e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        Message msg = new Message();
        msg.setSentTo(userSentTo);
        msg.setId(messageId);
        try {
            messageService.markMessageAsReaded(msg);
        } catch (UserNotFoundException e) {
            e.printStackTrace();
        } catch (MessageNotFoundException e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity<>(HttpStatus.OK);

    }


}

package pl.ntp.project.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.ntp.project.exception.UserAlreadyExistsException;
import pl.ntp.project.exception.UserNotFoundException;
import pl.ntp.project.exception.UserWrongCredentialsException;
import pl.ntp.project.model.Converters;
import pl.ntp.project.model.User;
import pl.ntp.project.model.dto.UserDTO;
import pl.ntp.project.model.service.UserService;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Łukasz Patro
 * on 16.06.17.
 */
@CrossOrigin
@RestController
@RequestMapping("/users")
public class UserController {

    @Autowired
    private UserService userService;

    @PostMapping
    public ResponseEntity<UserDTO> addUser(@RequestBody UserDTO userDTO) {

        User user = Converters.toUser(userDTO);

        if (user.getLogin() == null && user.getSocialId() == null) {
            return new ResponseEntity<>(userDTO, HttpStatus.BAD_REQUEST);
        }

        try {
            userService.addUser(user);
            return new ResponseEntity<>(userDTO, HttpStatus.OK);
        } catch (UserAlreadyExistsException e) {
            System.out.println(e.getMessage());
            return new ResponseEntity<>(userDTO, HttpStatus.CONFLICT);
        } catch (UserWrongCredentialsException e) {
            System.out.println(e.getMessage());
            return new ResponseEntity<>(userDTO, HttpStatus.BAD_REQUEST);
        }


    }

    @GetMapping
    public ResponseEntity<List<UserDTO>> getUsers() {

        try {
            List<UserDTO> list = userService.getUsersAll().stream().map(Converters::toUserDTO).collect(Collectors.toList());
            return new ResponseEntity<>(list, HttpStatus.OK);
        } catch (UserNotFoundException e) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }

    }

    @GetMapping(path = "/{id}")
    public ResponseEntity<UserDTO> getUserById(@PathVariable Long id) {

        try {
            UserDTO userDTO = Converters.toUserDTO(userService.getUserById(id));
            return new ResponseEntity<>(userDTO, HttpStatus.OK);
        } catch (UserNotFoundException e) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }

    }

    @GetMapping(path = "/login/{login}")
    public ResponseEntity<UserDTO> getUserByLogin(@PathVariable String login) {

        try {
            UserDTO userDTO = Converters.toUserDTO(userService.getUserByLogin(login));
            return new ResponseEntity<>(userDTO, HttpStatus.OK);
        } catch (UserNotFoundException e) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }

    }

    @GetMapping(path = "/socialId/{socialId}")
    public ResponseEntity<UserDTO> getUserBySocialId(@PathVariable Long socialId) {

        try {
            UserDTO userDTO = Converters.toUserDTO(userService.getUserBySocialId(socialId));
            return new ResponseEntity<>(userDTO, HttpStatus.OK);
        } catch (UserNotFoundException e) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }

    }


    @PutMapping(path = "/{id}")
    public ResponseEntity<UserDTO> putUser(@RequestBody UserDTO userDTO, @PathVariable Long id) {

        User user = Converters.toUser(userDTO);
        user.setId(id);
        userService.putUser(user);
        return new ResponseEntity<>(userDTO, HttpStatus.OK);

    }

    @DeleteMapping(path = "/{id}")
    public ResponseEntity<UserDTO> deleteUser(@PathVariable Long id) {

        try {
            userService.deleteUser(id);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (UserNotFoundException e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

    }


}

package pl.ntp.project.controller;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.ntp.project.exception.UserNotFoundException;
import pl.ntp.project.model.User;
import pl.ntp.project.model.dto.UserDTO;
import pl.ntp.project.model.service.UserService;
import javax.servlet.ServletException;
import java.util.Date;


/**
 * Created by Łukasz Patro
 * on 17.06.17.
 */
@CrossOrigin
@RestController
@RequestMapping("/login")
public class LoginController {

    @Autowired
    private UserService userService;

    @PostMapping
    public ResponseEntity<String> login(@RequestBody UserDTO userDTO) throws ServletException {

        String jwtToken;

        if (userDTO.getLogin() == null || userDTO.getPassword() == null) {
            return new ResponseEntity<>("Please send both login and password.", HttpStatus.BAD_REQUEST);
        }

        String login = userDTO.getLogin();
        String password = userDTO.getPassword();

        User user;
        try {
            user = userService.getUserByLogin(userDTO.getLogin());
        } catch (UserNotFoundException e) {
            //return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
            return new ResponseEntity<>("Invalid login. Please check your login and password.", HttpStatus.BAD_REQUEST);
        }

        if (!password.equals( user.getPassword() )) {
            return new ResponseEntity<>("Invalid login. Please check your login and password.", HttpStatus.BAD_REQUEST);
        }

        jwtToken = Jwts.builder().setSubject(login).claim("roles", "USER").setIssuedAt(new Date())
                .signWith(SignatureAlgorithm.HS256, "tajneprzezpoufne").compact();

        return new ResponseEntity<>(jwtToken, HttpStatus.OK);
    }

}

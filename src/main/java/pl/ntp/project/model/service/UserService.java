package pl.ntp.project.model.service;

import pl.ntp.project.exception.UserAlreadyExistsException;
import pl.ntp.project.exception.UserNotFoundException;
import pl.ntp.project.exception.UserWrongCredentialsException;
import pl.ntp.project.model.User;

import java.util.List;

/**
 * Created by Łukasz Patro
 * on 14.04.17.
 */
public interface UserService {

    void addUser(User user) throws UserAlreadyExistsException, UserWrongCredentialsException;

    List<User> getUsersAll() throws UserNotFoundException;

    User getUserById(Long id) throws UserNotFoundException;

    User getUserByLogin(String login) throws UserNotFoundException;

    User getUserBySocialId(Long socialId) throws UserNotFoundException;

    List<User> getUsersByFirstName(String firstName) throws UserNotFoundException;

    List<User> getUsersByLastName(String lastName) throws UserNotFoundException;

    void putUser(User user);

    void deleteUser(User user) throws UserNotFoundException;

    void deleteUser(Long id) throws UserNotFoundException;

}

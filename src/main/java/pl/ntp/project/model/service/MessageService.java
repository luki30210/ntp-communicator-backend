package pl.ntp.project.model.service;

import pl.ntp.project.exception.MessageNotFoundException;
import pl.ntp.project.exception.MessageWrongFormatException;
import pl.ntp.project.exception.UserNotFoundException;
import pl.ntp.project.model.Message;
import pl.ntp.project.model.User;

import java.util.List;

/**
 * Created by Łukasz Patro
 * on 18.06.17.
 */
public interface MessageService {

    Message getMessage(Long id) throws MessageNotFoundException;

    Message getMessage(Long id, Long userId) throws MessageNotFoundException, UserNotFoundException;

    void sendMessage(Message msg) throws MessageWrongFormatException, UserNotFoundException;

    List<Message> getUsersSentMessages(Long userId, Integer pageNumber, Integer messagesPerPage) throws UserNotFoundException, MessageNotFoundException;

    List<Message> getUsersReceivedMessages(Long userId, Integer pageNumber, Integer messagesPerPage) throws UserNotFoundException, MessageNotFoundException;

    List<Message> getUsersReceivedMessagesFromSender(Long userId, Long userSenderId, Integer pageNumber, Integer messagesPerPage) throws UserNotFoundException, MessageNotFoundException;

    List<Message> getUsersUnreadMessages(Long userId, Integer pageNumber, Integer messagesPerPage) throws UserNotFoundException, MessageNotFoundException;

    List<Message> getConversation(Long user1Id, Long user2Id, Integer pageNumber, Integer messagesPerPage) throws UserNotFoundException, MessageNotFoundException;

    List<Message> getConversationUnread(Long sentTo, Long sentBy, Integer pageNumber, Integer messagesPerPage) throws UserNotFoundException, MessageNotFoundException;

    List<Message> getAllUsersUnreadMessages(Long userId) throws UserNotFoundException, MessageNotFoundException;

    void markMessageAsReaded(Message msg) throws UserNotFoundException, MessageNotFoundException;

}
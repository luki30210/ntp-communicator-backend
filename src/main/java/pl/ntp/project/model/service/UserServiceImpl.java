package pl.ntp.project.model.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.ntp.project.exception.UserAlreadyExistsException;
import pl.ntp.project.exception.UserNotFoundException;
import pl.ntp.project.exception.UserWrongCredentialsException;
import pl.ntp.project.model.User;
import pl.ntp.project.model.repository.UserRepository;
import java.util.List;

/**
 * Created by Łukasz Patro
 * on 14.04.17.
 */
@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public void addUser(User user) throws UserAlreadyExistsException, UserWrongCredentialsException {

        String login = user.getLogin();
        Long socialId = user.getSocialId();

        if (login != null && user.getPassword() !=null && socialId == null) {

            User userOldByLogin = userRepository.findByLogin(login);
            if (userOldByLogin == null) {
                userRepository.save(user);
            } else {
                throw new UserAlreadyExistsException("User with login " + login + " already exists.");
            }

        } else if (socialId != null) {

            User userOldBySocialId = userRepository.findBySocialId(socialId);
            if (userOldBySocialId == null) {
                userRepository.save(user);
            } else {
                throw new UserAlreadyExistsException("User with social id " + socialId + " already exists.");
            }

        } else {

            throw new UserWrongCredentialsException("Wrong data.");

        }

    }

    @Override
    public List<User> getUsersAll() throws UserNotFoundException {

        List<User> list = userRepository.findAll();
        if (list == null || list.isEmpty()) {
            throw new UserNotFoundException("No users found.");
        } else {
            return list;
        }

    }

    @Override
    public User getUserById(Long id) throws UserNotFoundException {

        User user = userRepository.findOne(id);

        if (user == null) {
            throw new UserNotFoundException("User with id " + id + " doesn't exist");
        } else {
            return user;
        }

    }

    @Override
    public User getUserByLogin(String login) throws UserNotFoundException {

        User user = userRepository.findByLogin(login);

        if (user == null) {
            throw new UserNotFoundException("User with login " + login + " doesn't exist");
        } else {
            return user;
        }

    }

    @Override
    public User getUserBySocialId(Long socialId) throws UserNotFoundException {

        User user = userRepository.findBySocialId(socialId);

        if (user == null) {
            throw new UserNotFoundException("User with social id " + socialId + " doesn't exist");
        } else {
            return user;
        }

    }

    @Override
    public List<User> getUsersByFirstName(String firstName) throws UserNotFoundException {

        List<User> list = userRepository.findByFirstName(firstName);
        if (list == null || list.isEmpty()) {
            throw new UserNotFoundException("No users with first name " + firstName + ".");
        } else {
            return list;
        }

    }

    @Override
    public List<User> getUsersByLastName(String lastName) throws UserNotFoundException {

        List<User> list = userRepository.findByLastName(lastName);
        if (list == null || list.isEmpty()) {
            throw new UserNotFoundException("No users with last name " + lastName + ".");
        } else {
            return list;
        }

    }

    @Override
    public void putUser(User user) {

        userRepository.save(user);

    }

    @Override
    public void deleteUser(User user) throws UserNotFoundException {

        this.deleteUser(user.getId());

    }

    @Override
    public void deleteUser(Long id) throws UserNotFoundException {

        this.getUserById(id);
        userRepository.delete(id);

    }

}

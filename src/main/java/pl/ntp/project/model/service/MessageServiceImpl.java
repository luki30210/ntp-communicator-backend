package pl.ntp.project.model.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import pl.ntp.project.exception.MessageNotFoundException;
import pl.ntp.project.exception.MessageWrongFormatException;
import pl.ntp.project.exception.UserNotFoundException;
import pl.ntp.project.model.Message;
import pl.ntp.project.model.User;
import pl.ntp.project.model.repository.MessageRepository;
import pl.ntp.project.model.repository.UserRepository;

import java.awt.print.PageFormat;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by Łukasz Patro
 * on 18.06.17.
 */
@Service
public class MessageServiceImpl implements MessageService {

    @Autowired
    private MessageRepository messageRepository;

    @Autowired
    private UserRepository userRepository;

    @Override
    public Message getMessage(Long id) throws MessageNotFoundException {

        Message msg =  messageRepository.findOne(id);
        if (msg == null) {
            throw new MessageNotFoundException("Message doesn't exist.");
        }
        return msg;

    }

    @Override
    public Message getMessage(Long id, Long userId) throws MessageNotFoundException, UserNotFoundException {

        User user = this.getUser(userId);

        Message msg =  messageRepository.findByIdAndSentBy(userId, user);
        if (msg == null) {
            throw new MessageNotFoundException("Message doesn't exist.");
        }
        return msg;

    }

    @Override
    public void sendMessage(Message msg) throws MessageWrongFormatException, UserNotFoundException {

        User userSentBy = this.getUser(msg.getSentBy().getId());
        User userSentTo = this.getUser(msg.getSentTo().getId());

        if ( msg.getSentBy() == msg.getSentTo() ) {
            throw new MessageWrongFormatException("Loop message detected.");
        }
        messageRepository.save(msg);

    }

    @Override
    public List<Message> getUsersSentMessages(Long userId, Integer pageNumber, Integer messagesPerPage) throws UserNotFoundException, MessageNotFoundException {

        User user = this.getUser(userId);

        List<Message> messages = messageRepository.findBySentBy(user, new PageRequest(pageNumber, messagesPerPage)).getContent();
        if (messages == null) {
            throw new MessageNotFoundException("There is no read messages.");
        }

        return messages;

    }

    @Override
    public List<Message> getUsersReceivedMessages(Long userId, Integer pageNumber, Integer messagesPerPage) throws UserNotFoundException, MessageNotFoundException {

        User user = this.getUser(userId);

        List<Message> messages = messageRepository.findBySentToAndIsReceivedTrue(user, new PageRequest(pageNumber, messagesPerPage)).getContent();
        if (messages == null) {
            throw new MessageNotFoundException("There is no read messages.");
        }

        return messages;

    }

    @Override
    public List<Message> getUsersReceivedMessagesFromSender(Long userId, Long userSenderId, Integer pageNumber, Integer messagesPerPage) throws UserNotFoundException, MessageNotFoundException {

        User user = this.getUser(userId);
        User userSender = this.getUser(userSenderId);

        List<Message> messages = messageRepository.findBySentToAndSentByAndIsReceivedTrue(user, userSender, new PageRequest(pageNumber, messagesPerPage)).getContent();
        if (messages == null) {
            throw new MessageNotFoundException("There is no read messages.");
        }

        return messages;

    }

    @Override
    public List<Message> getConversation(Long userId1, Long userId2, Integer pageNumber, Integer messagesPerPage) throws UserNotFoundException, MessageNotFoundException {

        User user1 = this.getUser(userId1);
        User user2 = this.getUser(userId2);

        List<User> users = new ArrayList<>();
        users.add(user1);
        users.add(user2);

        Pageable page = new PageRequest(pageNumber, messagesPerPage);


        List<Message> messages = messageRepository.findBySentToInAndSentByInOrderByTimestampDesc(users, users, page).getContent();

        if (messages == null) {
            throw new MessageNotFoundException("There is no messages.");
        }

        return messages;

    }

    @Override
    public List<Message> getConversationUnread(Long sentTo, Long sentBy, Integer pageNumber, Integer messagesPerPage) throws UserNotFoundException, MessageNotFoundException {

        User userSentTo = this.getUser(sentTo);
        User userSentBy = this.getUser(sentBy);


        Pageable page = new PageRequest(pageNumber, messagesPerPage);


        List<Message> messages = messageRepository.findBySentToAndSentByAndIsReceivedFalseOrderByTimestampDesc(userSentTo, userSentBy, page).getContent();

        if (messages == null) {
            throw new MessageNotFoundException("There is no messages.");
        }

        return messages;

    }

    @Override
    public List<Message> getUsersUnreadMessages(Long userId, Integer pageNumber, Integer messagesPerPage) throws UserNotFoundException, MessageNotFoundException {

        User user = this.getUser(userId);

        Pageable page = new PageRequest(pageNumber, messagesPerPage);

        List<Message> messages = messageRepository.findBySentToAndIsReceivedFalseOrderByTimestampAsc(user, page).getContent();
        if (messages == null) {
            throw new MessageNotFoundException("There is no unread messages.");
        }

        return messages;

    }

    @Override
    public List<Message> getAllUsersUnreadMessages(Long userId) throws UserNotFoundException, MessageNotFoundException {

        User user = this.getUser(userId);

        List<Message> messages = messageRepository.findBySentToAndIsReceivedFalseOrderByTimestampAsc(user);
        if (messages == null) {
            throw new MessageNotFoundException("There is no unread messages.");
        }

        return messages;

    }

    @Override
    public void markMessageAsReaded(Message msg) throws UserNotFoundException, MessageNotFoundException {

        Message msgFromDB = this.getMessage(msg.getId());   //throws exception
        User user = userRepository.findOne(msg.getSentTo().getId());

        if (user == null) {
            throw new UserNotFoundException("User doesn't exist.");
        } else {
            msgFromDB.setIsReceived(true);
            messageRepository.save(msgFromDB);
        }

    }

    private User getUser(Long userId) throws UserNotFoundException {
        User user = userRepository.findOne(userId);
        if (user == null) {
            throw new UserNotFoundException("User with id " + user.getId() + " doesn't exist.");
        }
        return user;
    }

}

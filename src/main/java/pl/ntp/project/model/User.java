package pl.ntp.project.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.Set;

/**
 * Created by Łukasz Patro
 * on 08.04.17.
 */
@Entity
@Table(name = "users")
public class User {

    @Id
    //@NotNull
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotNull
    private String password;

    private String login;

    private Long socialId;

    @NotNull
    private String firstName;

    @NotNull
    private String lastName;

    private Date dateOfBirth;

    private String email;

    @OneToMany(mappedBy = "sentBy", cascade = CascadeType.ALL)
    private Set<Message> messagesSentBy;

    @OneToMany(mappedBy = "sentTo", cascade=CascadeType.ALL)
    private Set<Message> messagesSentTo;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public Long getSocialId() {
        return socialId;
    }

    public void setSocialId(Long socialId) {
        this.socialId = socialId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Set<Message> getMessagesSentBy() {
        return messagesSentBy;
    }

    public void setMessagesSentBy(Set<Message> messagesSentBy) {
        this.messagesSentBy = messagesSentBy;
    }

    public Set<Message> getMessagesSentTo() {
        return messagesSentTo;
    }

    public void setMessagesSentTo(Set<Message> messagesSentTo) {
        this.messagesSentTo = messagesSentTo;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}

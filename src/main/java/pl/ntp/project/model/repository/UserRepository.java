package pl.ntp.project.model.repository;


import org.springframework.data.repository.CrudRepository;
import pl.ntp.project.model.User;
import java.util.List;

/**
 * Created by Łukasz Patro
 * on 09.04.17.
 */
public interface UserRepository extends CrudRepository<User, Long> {

    @Override
    User findOne(Long id);

    User findByLogin(String login);

    User findBySocialId(Long socialId);

    @Override
    List<User> findAll();

    List<User> findByFirstName(String firstName);

    List<User> findByLastName(String lastName);


}

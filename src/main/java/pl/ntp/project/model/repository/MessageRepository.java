package pl.ntp.project.model.repository;


import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import pl.ntp.project.model.Message;
import pl.ntp.project.model.User;

import java.util.Collection;
import java.util.List;

/**
 * Created by Łukasz Patro
 * on 09.04.17.
 */
public interface MessageRepository extends CrudRepository<Message, Long> {

    @Override
    Message findOne(Long id);

    Message findByIdAndSentBy(Long id, User sentBy);

    //Page<Message> findAll(Pageable pageable);

    Page<Message> findBySentByAndSentTo(User sentBy, User sentTo, Pageable pageable);

    Page<Message> findBySentBy(User sentBy, Pageable pageable);

    List<Message> findBySentToAndIsReceivedFalseOrderByTimestampAsc(User sentTo);

    Page<Message> findBySentToAndIsReceivedFalseOrderByTimestampAsc(User sentTo, Pageable pageable);

    Page<Message> findBySentToAndSentByAndIsReceivedFalseOrderByTimestampDesc(User sentTo, User sentBy, Pageable pageable);

    //List<Message> findBySentToAndIsReceivedTrue(User sentTo);

    Page<Message> findBySentToAndIsReceivedTrue(User sentTo, Pageable pageable);

    Page<Message> findBySentToAndSentByAndIsReceivedTrue(User sentTo, User sentBy, Pageable pageable);

    //Page<Message> findBySentToIn(Collection<User> sentTo, Pageable pageable);

    Page<Message> findBySentToInAndSentByInOrderByTimestampDesc(Collection<User> sentTo, Collection<User> sentBy, Pageable pageable);

}

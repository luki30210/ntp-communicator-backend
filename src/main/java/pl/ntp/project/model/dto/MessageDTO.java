package pl.ntp.project.model.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import pl.ntp.project.model.User;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * Created by Łukasz Patro
 * on 16.06.17.
 */
public class MessageDTO {

    @Id
    private Long id;

    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private Long sentBy;

    @NotNull
    private Long sentTo;

    //@JsonFormat(pattern="dd-MM-yyyy HH:mm:ss")
    private Date timestamp;

    private Boolean isReceived;

    @NotNull
    private String content;

    public MessageDTO () {
        this.isReceived = false;
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getSentBy() {
        return sentBy;
    }

    public void setSentBy(Long sentBy) {
        this.sentBy = sentBy;
    }

    public Long getSentTo() {
        return sentTo;
    }

    public void setSentTo(Long sentTo) {
        this.sentTo = sentTo;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    public Boolean getIsReceived() {
        return isReceived;
    }

    public void setIsReceived(Boolean isReceived) {
        this.isReceived = isReceived;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}

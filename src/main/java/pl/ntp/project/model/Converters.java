package pl.ntp.project.model;

import org.springframework.beans.factory.annotation.Autowired;
import pl.ntp.project.model.dto.MessageDTO;
import pl.ntp.project.model.dto.UserDTO;
import pl.ntp.project.model.repository.UserRepository;
import pl.ntp.project.model.service.UserService;

/**
 * Created by Łukasz Patro
 * on 07.05.17.
 */
public class Converters {

    public static User toUser(UserDTO userDTO) {

        User user = new User();

        user.setFirstName(userDTO.getFirstName());
        user.setLastName(userDTO.getLastName());
        user.setEmail(userDTO.getEmail());
        user.setDateOfBirth(userDTO.getDateOfBirth());
        user.setLogin(userDTO.getLogin());
        user.setSocialId(userDTO.getSocialId());
        user.setPassword(userDTO.getPassword());

        return user;

    }

    public static UserDTO toUserDTO(User user) {

        UserDTO userDTO = new UserDTO();

        userDTO.setFirstName(user.getFirstName());
        userDTO.setLastName(user.getLastName());
        userDTO.setEmail(user.getEmail());
        userDTO.setDateOfBirth(user.getDateOfBirth());
        userDTO.setLogin(user.getLogin());
        userDTO.setSocialId(user.getSocialId());

        return userDTO;

    }

    public static Message toMessage(MessageDTO messageDTO) {

        Message message = new Message();
        User userSentBy = new User();
        User userSentTo = new User();
        userSentBy.setId(messageDTO.getSentBy());
        userSentTo.setId(messageDTO.getSentTo());

        message.setId(messageDTO.getId());
        message.setIsReceived(messageDTO.getIsReceived());
        message.setSentBy(userSentBy);
        message.setSentTo(userSentTo);
        message.setTimestamp(messageDTO.getTimestamp());
        message.setContent(messageDTO.getContent());

        return message;

    }

    public static MessageDTO toMessageDTO(Message message) {

        MessageDTO messageDTO = new MessageDTO();

        messageDTO.setId(message.getId());
        messageDTO.setIsReceived(message.getIsReceived());
        messageDTO.setSentBy(message.getSentBy().getId());
        messageDTO.setSentTo(message.getSentTo().getId());
        messageDTO.setTimestamp(message.getTimestamp());
        messageDTO.setContent(message.getContent());

        return messageDTO;

    }

}

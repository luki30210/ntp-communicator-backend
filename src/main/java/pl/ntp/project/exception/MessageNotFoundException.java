package pl.ntp.project.exception;

/**
 * Created by Łukasz Patro
 * on 11.05.17.
 */
public class MessageNotFoundException extends Exception {

    public MessageNotFoundException(String reason) {
        super(reason);
    }

}

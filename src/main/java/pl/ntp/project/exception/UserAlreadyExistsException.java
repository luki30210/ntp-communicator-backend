package pl.ntp.project.exception;

/**
 * Created by Łukasz Patro
 * on 11.05.17.
 */
public class UserAlreadyExistsException extends Exception {

    public UserAlreadyExistsException(String reason) {
        super(reason);
    }

}

package pl.ntp.project.exception;

/**
 * Created by Łukasz Patro
 * on 11.05.17.
 */
public class MessageWrongFormatException extends Exception {

    public MessageWrongFormatException(String reason) {
        super(reason);
    }

}

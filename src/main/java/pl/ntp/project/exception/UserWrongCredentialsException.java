package pl.ntp.project.exception;

/**
 * Created by Łukasz Patro
 * on 11.05.17.
 */
public class UserWrongCredentialsException extends Exception {

    public UserWrongCredentialsException(String reason) {
        super(reason);
    }

}

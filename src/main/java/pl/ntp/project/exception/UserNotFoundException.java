package pl.ntp.project.exception;

/**
 * Created by Łukasz Patro
 * on 11.05.17.
 */
public class UserNotFoundException extends Exception {

    public UserNotFoundException(String reason) {
        super(reason);
    }

}
